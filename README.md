Colorschemes Scripts Collection
=====================

Attribute to this ANSI Color thread.

*   [Crunchbang » ANSI colorschemes scripts](http://crunchbang.org/forums/viewtopic.php?id=13645)

Great, Superior, Fantastic, Incredible, Excellent, Amazing, Remarkable, Wonderful, Marvellous

-- -- --

Changes
----------

2018 June: Each script with preview image.
